package com.movie.apiMovie.repository;

import com.movie.apiMovie.entity.MovieEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("movieRepository")
public interface MovieRepository extends CrudRepository<MovieEntity, Long> {

    List<MovieEntity> findAll();

}
