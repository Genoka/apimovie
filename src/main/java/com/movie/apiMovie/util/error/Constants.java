package com.movie.apiMovie.util.error;

public enum Constants {

    /** Conection*/
    ACCEPT("application/json"),
    AUTHORIZATION("Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI2YjM5N2JlMzdmYjJhOTZmNWUxM2RiMzk1OTVjNjg1MyIsInN1YiI6IjY0NjI2N2EzZTNmYTJmMDEwM2ExZjVjMyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.p2FgGB6XIaVTZStGH3eaEA_XX2BMKxwes1j-urCrNWo"),
    LIST_MOVIES("https://api.themoviedb.org/3/movie/300/lists"),
    DETAIL_MOVIE("https://api.themoviedb.org/3/movie/");

    private String value;

    private Constants(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
