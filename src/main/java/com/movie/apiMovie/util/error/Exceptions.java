package com.movie.apiMovie.util.error;

import java.util.ArrayList;
import java.util.List;

/**
 * Title: Exceptions.java <br>
 */

// TODO LIB: Shorten some names...

public class Exceptions {

    public static final String PARAM_VACIO = "El parametro es null";
    public static final String ACTIVAR_PROPUESTA_MANUAL = "ActivarPropManual";
    public static final String ACTIVAR_PROPUESTA_RAPIDA = "ActivarPropRapida";
    public static final String RECHAZAR_PROPUESTA_COMERCIAL = "RechazarPropComercial";
    public static final String DESISTIR_PROPUESTA_COMERCIAL = "DesistirPropComercial";


    public static final String PROPUESTA_CANCELADA = "PropCanc";
    public static final String PROPUESTA_DESISTIDA = "PropDesis";

    public static final String COV_FALLECIM  = "FALLECIMIENTO";
    public static final String COV_EXONERAC  = "EXONERAC PAGO PRIMA INV TOTAL Y PERM POR ACC O ENF";
    public static final String COV_FONDOUNI  = "FONDO UNIVERSITARIO";
    public static final String COV_SOBREVIV  = "SOBREVIVENCIA";


    public List<String> toList(String[] array) {
        List<String> list = null;
        if (array==null || array.length == 0) list = new ArrayList(0);
        else {
            list = new ArrayList(array.length);
            for(int i = 0; i < array.length; i++) {
                list.add(array[i]);
            }
        }
        return list;
    }
}
