package com.movie.apiMovie.controller;

import com.movie.apiMovie.model.MovieModel;
import com.movie.apiMovie.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/apiMovie")
public class MovieController {

    @Autowired
    @Qualifier("movieServiceImpl")
    private MovieService movieService;

    @GetMapping(value = "moviesAll")
    public List<MovieModel> getAllMovies() {
        List<MovieModel> list = new ArrayList<MovieModel>();
        list = movieService.getAllMovies();
        return list;
    }

    @GetMapping(value = "/consulDetail/{id_movie}")
    public MovieModel consultUser(@PathVariable("id_movie") Long id_movie) throws NullPointerException {
        MovieModel MovieModel = movieService.getDetailMovie(id_movie);
        return MovieModel;
    }


    @PostMapping(value = "/addMovie")
    public ResponseEntity<Void> createUser(@RequestBody MovieModel newMovie) throws Exception {
        movieService.addMovie(newMovie);
        return new ResponseEntity.noContent().build();

    }


}
