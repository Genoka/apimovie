package com.movie.apiMovie.mapper;

import com.movie.apiMovie.entity.MovieEntity;
import com.movie.apiMovie.model.MovieModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 *
 */

@Component
public class MovieMapper {

    public MovieMapper() {
    }

/*
    public MovieEntity toEntity(MovieModel model) {
        MovieEntity entity = new MovieEntity();
        BeanUtils.copyProperties(model, entity);
        return entity;
    }


    public MovieModel toDto(MovieEntity entity) {
        MovieModel model = new MovieModel();
        BeanUtils.copyProperties(entity, model);
        return model;
    }

    public static MovieEntity toStaticEntity(MovieModel model) {
        MovieEntity entity = new MovieEntity();
        BeanUtils.copyProperties(model, entity);
        return entity;
    }

    public static MovieModel toStaticDto(MovieEntity entity) {
        MovieModel model = new MovieModel();
        BeanUtils.copyProperties(entity, model);
        return model;
    }

    public static List<MovieEntity> toListEntity(List<MovieModel> listModel) {
        return listModel.stream().map(MovieMapper::toStaticEntity).collect(Collectors.toList());
    }

    public static List<MovieModel> toListDto(List<MovieEntity> listEntity) {
        return listEntity.stream().map(MovieMapper::toStaticDto).collect(Collectors.toList());
    }
    */

}
