package com.movie.apiMovie.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;

@Entity
public class MovieEntity {

    private static final long serialVersionUID = 1L;

    @Column(name = "id")
    private Long id;
    @Column(name = "description")
    private String description;
    @Column(name = "favorite_count")
    private Integer favorite_count;
    @Column(name = "item_count")
    private Integer item_count;
    @Column(name = "iso_639_1")
    private String iso_639_1;
    @Column(name = "list_type")
    private String list_type;
    @Column(name = "name")
    private String name;
    @Column(name = "poster_path")
    private String poster_path;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getFavorite_count() {
        return favorite_count;
    }

    public void setFavorite_count(Integer favorite_count) {
        this.favorite_count = favorite_count;
    }

    public Integer getItem_count() {
        return item_count;
    }

    public void setItem_count(Integer item_count) {
        this.item_count = item_count;
    }

    public String getIso_639_1() {
        return iso_639_1;
    }

    public void setIso_639_1(String iso_639_1) {
        this.iso_639_1 = iso_639_1;
    }

    public String getList_type() {
        return list_type;
    }

    public void setList_type(String list_type) {
        this.list_type = list_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }
}
