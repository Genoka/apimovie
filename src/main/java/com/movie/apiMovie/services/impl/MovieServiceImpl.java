package com.movie.apiMovie.services.impl;


import com.movie.apiMovie.entity.MovieEntity;
import com.movie.apiMovie.mapper.MovieMapper;
import com.movie.apiMovie.model.MovieModel;
import com.movie.apiMovie.repository.MovieRepository;
import com.movie.apiMovie.services.MovieService;
import com.movie.apiMovie.util.error.Constants;
import com.movie.apiMovie.util.error.Exceptions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Optional;

@Service("movieServiceImpl")
public class MovieServiceImpl implements MovieService {


    /**
     * The logger.
     */
    public static final Logger logger = LoggerFactory.getLogger(MovieServiceImpl.class);
    /**
     * Variable
     */
    URL url = null;

    @Autowired
    private MovieRepository repository;

    @Autowired
    private MovieMapper mapper;

    /**
     * Busca todas las peliculas.
     *
     * @return List<MovieModel>
     */
    @Override
    public List<MovieModel> getAllMovies() {

        logger.info("Inicia Busqueda de lista de movies");
        List<MovieModel> moviesList = null;
        HttpURLConnection connection = null;
        String responseJson = null;
        try {
            url = new URL(Constants.LIST_MOVIES.getValue());

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization", Constants.AUTHORIZATION.getValue());
            connection.setRequestProperty("Accept", Constants.ACCEPT.getValue());

            if (connection.getResponseCode() != 200) {
                throw new RuntimeException("HTTP error code : "
                        + connection.getResponseCode());
            }

            InputStreamReader inputStreamReader = new InputStreamReader(connection.getInputStream());
            BufferedReader reader = new BufferedReader(inputStreamReader);
            responseJson = reader.readLine();

            JSONObject jsonObject = new JSONObject(responseJson);
            JSONArray jsonArray = jsonObject.getJSONArray("results");
            MovieModel model = new MovieModel();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                //Aquí se obtiene el dato y es guardado en una lista
                model.setId(json.getLong("id"));
                model.setDescription(json.getString("description"));
                model.setFavorite_count(json.getInt("favorite_count"));
                model.setIso_639_1(json.getString("iso_639_1"));
                model.setItem_count(json.getInt("item_count"));
                model.setName(json.getString("name"));
                model.setList_type(json.getString("list_type"));
                model.setPoster_path(json.getString("poster_path"));

                moviesList.add(model);

            }

            System.out.println("Jason: string:" + jsonArray.toString());


        } catch (MalformedURLException e) {
            logger.error("Error obteniendo la Url ", e.getMessage());
        } catch (IOException e) {
            logger.error("Error abriendo conexion ", e.getMessage());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return moviesList;
    }


    /**
     * Metodo que permite obtener el detalle de una pelicula
     * @param idMovie
     * @return MovieModel
     */
    @Override
    public MovieModel getDetailMovie(Long idMovie) throws NullPointerException{

        if(idMovie == 0L){
            throw new NullPointerException(Exceptions.PARAM_VACIO + idMovie.toString());

        }
        logger.info("Inicia obtener detalle");
        HttpURLConnection connection = null;
        String responseJson = null;
        MovieModel model = new MovieModel();
        try {
            url = new URL(Constants.LIST_MOVIES.getValue()+idMovie.toString());

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization", Constants.AUTHORIZATION.getValue());
            connection.setRequestProperty("Accept", Constants.ACCEPT.getValue());

            if (connection.getResponseCode() != 200) {
                throw new RuntimeException("HTTP error code : "
                        + connection.getResponseCode());
            }

            InputStreamReader inputStreamReader = new InputStreamReader(connection.getInputStream());
            BufferedReader reader = new BufferedReader(inputStreamReader);
            responseJson = reader.readLine();

            JSONObject jsonObject = new JSONObject(responseJson);

            //Aquí se obtiene el dato y es guardado en una lista
            model.setId(jsonObject.getLong("id"));
            model.setDescription(jsonObject.getString("description"));
            model.setFavorite_count(jsonObject.getInt("favorite_count"));
            model.setIso_639_1(jsonObject.getString("iso_639_1"));
            model.setItem_count(jsonObject.getInt("item_count"));
            model.setName(jsonObject.getString("name"));
            model.setList_type(jsonObject.getString("list_type"));
            model.setPoster_path(jsonObject.getString("poster_path"));


        } catch (MalformedURLException e) {
            logger.error("Error obteniendo la Url ", e.getMessage());
        } catch (IOException e) {
            logger.error("Error abriendo conexion ", e.getMessage());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return model;
    }


    @Override
    public void addMovie(MovieModel movieModel) {
        MovieEntity entity = new MovieEntity();
        Optional<MovieEntity> exist = repository.findById(movieModel.getId());
        try {
            if (exist == null) {
                entity.setDescription(movieModel.getDescription());
                entity.setFavorite_count(movieModel.getFavorite_count());
                entity.setIso_639_1(movieModel.getIso_639_1());
                entity.setItem_count(movieModel.getItem_count());
                entity.setList_type(movieModel.getList_type());
                entity.setName(movieModel.getName());
                entity.setPoster_path(movieModel.getPoster_path());
                repository.save(entity);
            }
        } catch (Exception e) {
            logger.error("Error processing insert", e.getMessage());
        }
    }

}
