package com.movie.apiMovie.services;

import com.movie.apiMovie.model.MovieModel;

import java.util.List;

public interface MovieService {

    List<MovieModel> getAllMovies();

    MovieModel getDetailMovie(Long idMovie);

    void addMovie(MovieModel movieModel);
}
